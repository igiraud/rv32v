#include "ac_int.h"
//#include <cstdint>
#include "memoryInterface.h"
#include "opcodes.h"
//#include "var_adder.h"

#define VLENP2 (7)
#define VLEN (1<<VLENP2)
#define VLLEN (VLENP2) // vlen/32 <= VLMAX <= vlen/2
#define XLEN 32
#define ELEN 32
#define VBUSW VLEN

#define SEW (1<<(3+VTYPE_GET_VSEW))
#define VTYPE_GET_VLMUL   vtype.slc<3>(0)
#define VTYPE_GET_VSEW    vtype.slc<2>(3)
#define VTYPE_GET_VTA     (vtype.slc<1>(6))
#define VTYPE_GET_VMA     (vtype.slc<1>(7))
#define VTYPE_GET_VILL    (vtype.slc<1>(XLEN-1))
#define VTYPE_SET_VILL(n) (vtype.set_slc<1>(XLEN-1, (ac_int<1,false>)n))

enum ScalTypes {SCAL_IMM, SCAL_RX, SCAL_RV};
enum OpeTypes {VV, VI, VX};
enum ALUOpe {NOP, ALU_VSETVL, ALU_ADD, ALU_MERGE};

typedef enum ScalTypes ScalTypes;
typedef enum OpeTypes OpeTypes;
typedef enum ALUOpe ALUOpe;
typedef struct RVVInterface RVVInterface;
//typedef struct DCInstr DCInstr;
typedef struct DCtoGet DCtoGet;
typedef struct GettoEx GettoEx;
typedef struct ExtoMem ExtoMem;
typedef struct MemtoWB MemtoWB;
typedef struct WBOut WBOut;
typedef struct RV32V RV32V;

struct RVVInterface {
  ac_int<32,false> instruction;
  ac_int<XLEN,false> r1, r2, rd;
};

struct DCtoGet {
  ac_int<32,false> instruction;
  ac_int<32,false> r1;
  ac_int<32,false> r2;
  ac_int<7,false> opcode;    // instruction[0:6]
  ac_int<3,false> funct3;    // instruction[12:14] also mem width
  ac_int<5,false> vs1;       // instruction[15:19]
  ac_int<5,false> imm5;      // instruction[15:19]
  ac_int<5,false> vs2;       // instruction[20:24]
  ac_int<5,false> lsumop;    // instruction[20:24]
  ac_int<10,false> imm10;    // instruction[20:29]
  ac_int<2,false> mop;       // instruction[26:27]
  ac_int<6,false> funct6;    // instruction[26:31]
  bool mew;                  // instruction[28]
  ac_int<3,false> nf;        // instruction[29:31]
  ac_int<2,false> vsetform;  // instruction[30:31]
  ac_int<3,false> loopcount;
  ac_int<VLLEN,false> root, evl, vl, vstart;
  ac_int<3,false> emul;
  bool swapOp12;
  bool newInstr;

  bool crashFlag;
  
  bool neg1;
  OpeTypes opeType;
  ALUOpe aluOpe;
  
  memOpType memType;
  
  ac_int<2,false> eew;
  ac_int<5,false> vd;        // instruction[7:11]
  bool vm, vmop, vmwb;                   // instruction[25]
  bool wbvtype, wbrd, wbvd;
  bool isValid;
};

struct GettoEx {
  ac_int<VLEN,false> op1;
  ac_int<VLEN,false> op2;
  ac_int<VLLEN,false> avl;
  bool neg1;
  OpeTypes opeType;
  ALUOpe aluOpe;
  
  memOpType memType;
  
  ac_int<2,false> eew;
  ac_int<5,false> vd;
  ac_int<VLEN,false> m0, mb;
  ac_int<VLEN,false> op3;
  ac_int<XLEN,false> vtype;
  bool vmop, vmwb;
  bool wbvtype, wbrd, wbvd;
  bool isValid;
};

struct ExtoMem {
  memOpType memType;
  ac_int<VLEN,false> res;

  ac_int<2,false> eew;
  ac_int<5,false> vd;
  ac_int<VLEN,false> m0;
  ac_int<VLEN,false> op3;
  ac_int<XLEN,false> vtype;
  bool wbvtype, wbrd, wbvd;
  bool isValid;
};

struct MemtoWB {
  ac_int<2,false> eew;
  ac_int<VLEN,false> res;
  ac_int<5,false> vd;
  ac_int<VLEN,false> m0;
  ac_int<VLEN,false> op3;
  ac_int<XLEN,false> vtype;
  bool wbvtype, wbrd, wbvd;
  bool isValid;
};

struct WBOut {
  ac_int<XLEN,false> rd;
  bool wbrd;
  bool isValid;
};


struct RV32V {
  RVVInterface* rvvInterface;
  DCtoGet dctoGet;
  GettoEx gettoEx;
  ExtoMem extoMem;
  MemtoWB memtoWB;

  bool aluStall, memStall, rvvStall;
  
  //DCInstr dcInstr;
  ac_int<VLEN,false> v_reg[32];
  
  MemoryInterface<4> *data_mem;
  ac_int<VLLEN,false> vstart, vl;
  ac_int<XLEN,false> vtype;
  ac_int<XLEN,false> vxstat, vxrm, vcsr, vlenb;
};
  
void initRV32V(RV32V* rv32v);
void doCycle(RV32V* rv32v, bool globalStall);
// Pipeline
void decode(RVVInterface rvvInterface, DCtoGet* dctoGet, ac_int<XLEN,false> vtype, ac_int<VLLEN,false> vl);
void get(DCtoGet dctoGet, GettoEx* gettoEx, ac_int<VLEN,false> v_reg[32], bool* rvvStall);
void execute(GettoEx gettoEx, ExtoMem* extoMem);
void memAccess(ExtoMem extoMem, MemtoWB* memtoWB);
void writeBack(MemtoWB memtoWB, WBOut* wbOut, ac_int<VLEN,false> v_reg[32], ac_int<XLEN,false>* vtype, ac_int<VLLEN,false>* vl);

// Opérateurs
ac_int<VLEN,false> complt2(ac_int<VBUSW,false> op1, ac_int<2,false> eew);
ac_int<VLEN,false> add(ac_int<VBUSW,false> op1, ac_int<VBUSW,false> op2, ac_int<2,false> eew);
ac_int<VLEN,false> setvl(ac_int<XLEN,false> vtypein, ac_int<VLLEN,false> avl,
                         ac_int<XLEN,false>* vtypeout);
ac_int<VLEN,false> makeMask(ac_int<VLEN/2,false> v0, ac_int<2,false> eew);
ac_int<VLEN,false> makeMaskBody(ac_int<VLLEN,false> root,
                                ac_int<VLLEN,false> vstart,
                                ac_int<VLLEN,false> evl, ac_int<2,false> eew);
ac_int<VLEN,false> extend(ac_int<XLEN,false> nb, ac_int<2,false> eew);
//ac_int<VLEN,false> load_store();



