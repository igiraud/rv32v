#include <cstdio>
#include <cstring>
#include <cstdint>
#include "opcodes.h"

#define MASK 0xFC00707F // Conserve les champs f6, f3 et opcode
#define codev(f6, f3) (f6<<26 | f3<<12 | OP_V)
#define F6_VSETVLI  0x00
#define F6_VSETIVLI 0x30
#define F6_VSETVL   0x20
#define vd(I) ((unsigned char)((I & 0x00000f80)>>7))
#define vs1(I) ((unsigned char)((I & 0x000f8000)>>15))
#define imm5(I) ((signed char)((((int32_t)I)<<12)>>27))
#define vs2(I) ((unsigned char)((I & 0x01f00000)>>20))
#define imm9(I) ((unsigned short)((I & 0x3ff00000)>>20))
#define mask(I) ((I&0x02000000) ? "" : ", v0.t")

void print_decode(unsigned int instruction);
