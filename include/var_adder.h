#include "ac_int.h"

// Additionneur avec retenue anticipée à largeur variable.

template <int LEN>
ac_int<LEN,false> var_adder(ac_int<LEN,false> A, ac_int<LEN,false> B, ac_int<1,false> C0, ac_int<1,false>* T10, ac_int<1,false>* G10, ac_int<2,false> vsew) {
  ac_int<1,false> C1, T1, G1, T0, G0;
  ac_int<LEN,false> retour;
  ac_int<LEN/2,false> Ab, At, Bb, Bt;
  ac_int<LEN/2,false> temp;

  Ab = A.template slc<LEN/2>(0);
  At = A.template slc<LEN/2>(LEN/2);
  Bb = B.template slc<LEN/2>(0);
  Bt = B.template slc<LEN/2>(LEN/2);

  temp = var_adder<LEN/2>(Ab, Bb, C0, &T0, &G0, vsew);
  retour.template set_slc<LEN/2>(0, temp);

  C1 = (C0 & T0) | G0;

  temp = var_adder<LEN/2>(At, Bt, C1, &T1, &G1, vsew);
  retour.template set_slc<LEN/2>(LEN/2, temp);

  *T10 = T1 & T0;
  *G10 = G1 | (G0 & T1);

  return retour;
}

template <>
ac_int<8,false> var_adder<8>(ac_int<8,false> A, ac_int<8,false> B, ac_int<1,false> C0, ac_int<1,false>* T10, ac_int<1,false>* G10, ac_int<2,false> vsew) {
  *T10 = ((A & B)==0xFF);
  *G10 = (A+B)>>8;
  return ((ac_int<8,false>)(A+B+C0));
}

template <>
ac_int<16,false> var_adder<16>(ac_int<16,false> A, ac_int<16,false> B, ac_int<1,false> C0, ac_int<1,false>* T10, ac_int<1,false>* G10, ac_int<2,false> vsew) {
  ac_int<1,false> C1, T1, G1, T0, G0;
  ac_int<16,false> retour;
  ac_int<8,false> Ab, At, Bb, Bt;
  ac_int<8,false> temp;

  Ab = A.template slc<8>(0);
  At = A.template slc<8>(8);
  Bb = B.template slc<8>(0);
  Bt = B.template slc<8>(8);

  temp = var_adder<8>(Ab, Bb, C0, &T0, &G0, vsew);
  retour.template set_slc<8>(0, temp);

  C1 = ((C0 & T0) | G0) && (vsew!=0);

  temp = var_adder<8>(At, Bt, C1, &T1, &G1, vsew);
  retour.template set_slc<8>(8, temp);

  *T10 = T1 & T0;
  *G10 = G1 | (G0 & T1);

  return retour;
}

template <>
ac_int<32,false> var_adder<32>(ac_int<32,false> A, ac_int<32,false> B, ac_int<1,false> C0, ac_int<1,false>* T10, ac_int<1,false>* G10, ac_int<2,false> vsew) {
  ac_int<1,false> C1, T1, G1, T0, G0;
  ac_int<32,false> retour;
  ac_int<16,false> Ab, At, Bb, Bt;
  ac_int<16,false> temp;

  Ab = A.template slc<16>(0);
  At = A.template slc<16>(16);
  Bb = B.template slc<16>(0);
  Bt = B.template slc<16>(16);

  temp = var_adder<16>(Ab, Bb, C0, &T0, &G0, vsew);
  retour.template set_slc<16>(0, temp);

  C1 = ((C0 & T0) | G0) && (vsew==2);

  temp = var_adder<16>(At, Bt, C1, &T1, &G1, vsew);
  retour.template set_slc<16>(16, temp);

  *T10 = T1 & T0;
  *G10 = G1 | (G0 & T1);

  return retour;
}
