
/* major OPCODES */
#define LOAD_FP  0x07
#define STORE_FP 0x27
#define OP_V     0x57


/* funct3 pour OP-V */
#define F3_OPIVV  0x0 // 0b000
#define F3_OPIVX  0x4 // 0b100
#define F3_OPIVI  0x3 // 0b011
#define F3_OPMVV  0x2 // 0b010
#define F3_OPMVX  0x6 // 0b110
#define F3_OPFVV  0x1 // 0b001  // Nombres flottants non-implémentés
#define F3_OPFVF  0x5 // 0b101  // Nombres flottants non-implémentés
#define F3_VSETVL 0x7 // 0b111


/* funct6 pour OPIV* */
#define F6_VADD        0x00 // V
#define F6_RES0x01
#define F6_VSUB        0x02
#define F6_VRSUB       0x03
#define F6_VMINU       0x04
#define F6_VMIN        0x05
#define F6_VMAXU       0x06
#define F6_VMAX        0x07
#define F6_RES0x08
#define F6_VAND        0x09
#define F6_VOR         0x0a
#define F6_VXOR        0x0b
#define F6_VRGATHER    0x0c
#define F6_RES0x0D     0x0d
#define F6_VSLIDEUP    0x0e
#define F6_VRGATHERI16 0x0e
#define F6_VSLIDEDOWN  0x0f
#define F6_VADC        0x10
#define F6_VMADC       0x11
#define F6_VSBC        0x12
#define F6_VMSBC       0x13
#define F6_RES0x14
#define F6_RES0x15
#define F6_RES0x16
#define F6_VMERGE      0x17
#define F6_VMV         0x17
#define F6_VMSEQ       0x18
#define F6_VMSNE       0x19
#define F6_VMSLTU      0x1a
#define F6_VMSLT       0x1b
#define F6_VMSLEU      0x1c
#define F6_VMSLGE      0x1d
#define F6_VMSGTU      0x1e
#define F6_VMSGT       0x1f
#define F6_VSADDU
#define F6_VSADD
#define F6_VSSUBU
#define F6_VSSUB
#define F6_RES0x24
#define F6_VSLL        0x25
#define F6_RES0x26
#define F6_VSMUL
#define F6_VMVnrR      0x27
#define F6_VSRL        0x28
#define F6_VSRA        0x29
#define F6_VSSRL
#define F6_VSSRA
#define F6_VNSRL
#define F6_VNSRA
#define F6_VNCLIPU
#define F6_VNCLIP
#define F6_VWREDSUMU
#define F6_VWREDSUM


/* funct6 pour OPMV* */
#define F6_VREDSUM
#define F6_VREDAND
#define F6_VREDOR
#define F6_VREDXOR
#define F6_VREDMINU
#define F6_VREDMIN
#define F6_VREDMAXU
#define F6_VREDMAX
#define F6_VAADDU
#define F6_VAADD
#define F6_VASUBU
#define F6_VASUB
#define F6_RES0x0c
#define F6_RES0x0d
#define F6_VSLIDE1UP
#define F6_VSLIDE1DOWN 
#define F6_VWXUNARY0    0x10  // OPMVV, vmv.x.s
#define F6_VRXUNARY0    0x10  // OPMVX, vmv.s.x
#define F6_RES0x11
#define F6_VXUNARY0
#define F6_RES0x13
#define F6_VMUNARY0
#define F6_RES0x15
#define F6_RES0x16
#define F6_VCOMPRESS
#define F6_VMANDNOT
#define F6_VMAND
#define F6_VMOR
#define F6_VMXOR
#define F6_VMORNOT
#define F6_VMNAND
#define F6_VMNOR
#define F6_VMXNOR
#define F6_VDIVU
#define F6_VDIV
#define F6_VREMU
#define F6_VREM
#define F6_VMULHU       0x24
#define F6_VMUL         0x25
#define F6_VMULHSU      0x26
#define F6_VMULH        0x27
#define F6_RES0x28
#define F6_VMADD        0x29
#define F6_RES0x2A
#define F6_VNMSUB
#define F6_RES0x2C
#define F6_VMACC        0x2d
#define F6_RES0x2E
#define F6_VNMSAC
#define F6_VWADDU
#define F6_VWADD
#define F6_VWSUBU
#define F6_VWSUB
#define F6_VWADDU_W
#define F6_VWSUB_W
#define F6_VWMULU
#define F6_RES0x39
#define F6_VWMULSU
#define F6_VWMUL
#define F6_VWMACCU
#define F6_VWMACC
#define F6_VWMACCUS
#define F6_VWMACCSU


/* mop pour LOAD-FP / STORE-FP */
#define MOP_VLS_E    0x0 // unit-stride
#define MOP_VLS_UXEI 0x1 // indexed-unordered
#define MOP_VLS_SE   0x2 // strided
#define MOP_VLS_OXEI 0x3 // indexed-ordered
