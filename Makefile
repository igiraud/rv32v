.POSIX:

CC = g++
#CFLAGS = -Wall -Wextra
LDFLAGS = -I "./include"

all: build_dir main

main: src/main.cpp objects/print_decode.o objects/rv32v.o 
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

objects/rv32v.o:src/rv32v.cpp
	$(CC) $(CFLAGS) $(LDFLAGS) -c -o $@ $^ $(LDLIBS)

objects/print_decode.o:src/print_decode.cpp
	g++ $(CFLAGS) $(LDFLAGS) -c -o $@ $^ $(LDLIBS)

objects/main.o: src/main.cpp
	$(CC) $(CFLAGS) $(LDFLAGS) -c -o $@ $^ $(LDLIBS)

build_dir:
	mkdir -p objects

clean:
	rm ./objects/*
	rm main
