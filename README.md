# Accélérateur RISC-V "V" pour HLS

Un coprocesseur vectoriel pour cœur RISC-V conçu pour la HLS (High Level Synthesis).

## Instructions implémentées

- `vsetvl`, `vsetvli`, `vsetivli`. sew = 8, 16 ou 32. lmul = 1, 2, 4 ou 8
- `vadd`, `vsub`, `vrsub`. Toutes les variantes `vv`, `vx` et `vi`.
- `vmerge`, `vmv` et variantes `vv`, `vx` et `vi`.
- `vmv.s.x` et `vmv.x.s`

## Compilation

Il est possible de compiler un simulateur permettant de tester les instructions avec
```
make
```
Des instructions de test peuvent être générée avec
```
cd tests
./run.sh N
cd ..
```
où N est le nombre d'instructions à générer.

Le simulateur peut ensuite être exécuté avec
```
./main tests/out.bin
```

## Synthèse

Le coprocesseur peut être synthétisé à l'aide d'outils de HLS. La synthèse RTL a été essayée avec [Mentor Catapult HLS](https://www.mentor.com/hls-lp/catapult-high-level-synthesis/)

## A faire
- Renvois et suspensions dans le pipeline. Actuellement, le résultat est incorrect.
- Accès mémoire
- Corriger le calcul du masque de corps (trop lent)
- Corriger les sorties qui empèchent la synthèse de se faire en un cycle.
- Améliorer l'algorithme de multiplication.
- Implémenter les instructions de multiplication.