#include "adder.h"

ac_int<VLEN,false> testadd(ac_int<VLEN,false> op1, ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype) {
  ac_int<3,false> vsew = vtype.slc<2>(3);
  ac_int<1,false> trash;
  ac_int<VLEN,false> retour;
  for (int i=0; i<VLEN; i+=32) {
    ac_int<32,false> A = op1.slc<32>(i);
    ac_int<32,false> B = op2.slc<32>(i);
    retour.set_slc<32>(i, adder(A,B,0,&trash,&trash,vsew));
  }
  return retour;
}

