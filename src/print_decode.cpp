#include "print_decode.h"

void print_decode_vsetvl(unsigned short vtypei, char* buffer);

char* str_sew[] = {"e8", "e16", "e32", "e64", "e128", "res", "res", "res"};
char* str_lmul[] = {"m1", "m2", "m4", "m8", "res", "mf8", "mf4", "mf2"};

void print_decode(unsigned int instruction) {
  static char mask_instruction[10] = ", v0.t";
  char vsetvl_buff[30];
  print_decode_vsetvl(imm9(instruction), vsetvl_buff);
  mask_instruction[0] = (instruction&0x02000000) ? '\0' : ',';
  printf("%08x : ", instruction);
  switch (instruction & MASK) {
  case codev(F6_VSETVLI,F3_VSETVL):
    printf("vsetvli     x%-2u%s\n", vs1(instruction), vsetvl_buff);
    break;
  case codev(F6_VSETIVLI,F3_VSETVL):
    printf("vsetivli    %u%s\n", vs1(instruction), vsetvl_buff);
    break;
  case codev(F6_VSETVL,F3_VSETVL):
    printf("vsetvl      x%-2u, x%-2u\n", vs1(instruction), vs2(instruction));
    break;
  case codev(F6_VADD,F3_OPIVV):
    printf("vadd.vv     v%-2u, v%-2u, v%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
  case codev(F6_VADD,F3_OPIVI):
    printf("vadd.vi     v%-2u, v%-2u, %i%s\n", vd(instruction), vs2(instruction), imm5(instruction),mask_instruction);
    break;
  case codev(F6_VADD,F3_OPIVX):
    printf("vadd.vx     v%-2u, v%-2u, x%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
    case codev(F6_VSUB,F3_OPIVV):
    printf("vsub.vv     v%-2u, v%-2u, v%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
    case codev(F6_VSUB,F3_OPIVI):
    printf("vsub.vi     v%-2u, v%-2u, %i%s\n", vd(instruction), vs2(instruction), imm5(instruction),mask_instruction);
    break;
  case codev(F6_VSUB,F3_OPIVX):
    printf("vsub.vx     v%-2u, v%-2u, x%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
  case codev(F6_VRSUB,F3_OPIVV):
    printf("vrsub.vv    v%-2u, v%-2u, v%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
  case codev(F6_VRSUB,F3_OPIVI):
    printf("vrsub.vi    v%-2u, v%-2u, %i%s\n", vd(instruction), vs2(instruction), imm5(instruction),mask_instruction);
    break;
  case codev(F6_VRSUB,F3_OPIVX):
    printf("vrsub.vx    v%-2u, v%-2u, x%-2u%s\n", vd(instruction), vs2(instruction), vs1(instruction),mask_instruction);
    break;
  case codev(F6_VMERGE,F3_OPIVV):
    if ((instruction&0x02000000)) {
      printf("vmv.v.v     v%-2u, v%-2u\n", vd(instruction), vs1(instruction));
    } else {
      printf("vmerge.vvm  v%-2u, v%-2u, v%-2u, v0\n", vd(instruction), vs2(instruction), vs1(instruction));
    }
    break;
  case codev(F6_VMERGE,F3_OPIVI):
    if ((instruction&0x02000000)) {
      printf("vmv.v.i     v%-2u, %i\n", vd(instruction), imm5(instruction));
    } else {
      printf("vmerge.vim  v%-2u, v%-2u, %i, v0\n", vd(instruction), vs2(instruction), imm5(instruction));
    }
    break;
  case codev(F6_VMERGE,F3_OPIVX):
    if ((instruction&0x02000000)) {
      printf("vmv.v.x     v%-2u, x%-2u\n", vd(instruction), vs1(instruction));
    } else {
      printf("vmerge.vxm  v%-2u, v%-2u, x%-2u, v0\n", vd(instruction), vs2(instruction), vs1(instruction));
    }
    break;
  case codev(F6_VWXUNARY0,F3_OPMVV):
  case codev(F6_VRXUNARY0,F3_OPMVX):
    printf("%08x\n", (instruction & MASK));
    break;
  default:
    printf("instruction non-supportée\n");
    break;
  }
}

void print_decode_vsetvl(unsigned short vtypei, char* buffer) {
  int len = 0;
  len+=sprintf(buffer+len, ", %s", str_sew[(vtypei>>3)&0x7]);
  len+=sprintf(buffer+len, ", %s", str_lmul[(vtypei)&0x7]);
  len+=sprintf(buffer+len, ", t%c", ((vtypei>>6)&0x1) ? 'a' : 'u');
  len+=sprintf(buffer+len, ", m%c", ((vtypei>>7)&0x1) ? 'a' : 'u');
}
