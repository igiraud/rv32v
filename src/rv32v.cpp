#include "rv32v.h"
#include <cstdio>
#include "var_adder.h"

using namespace std;

void initRV32V(RV32V* rv32v) {
  rv32v->vstart = 0;
  rv32v->vxstat = 0;
  rv32v->vl = 0;
  rv32v->vtype = (1<<(XLEN-1));
  rv32v->aluStall = false;
  rv32v->memStall = false;
  rv32v->rvvStall = false;
  rv32v->dctoGet.crashFlag = false;
  rv32v->dctoGet.vs1 = 0;
  rv32v->dctoGet.vs2 = 0;
  rv32v->dctoGet.vd = 0;
  rv32v->dctoGet.isValid = false;
  rv32v->gettoEx.isValid = false;
  rv32v->extoMem.isValid = false;
  rv32v->memtoWB.isValid = false;
  for (int i=0; i<32; i++) {
    rv32v->v_reg[i] = 0;
  }
  rv32v->vlenb = VLEN/8;
}

void decode(RVVInterface rvvInterface, DCtoGet* dctoGet,
            ac_int<XLEN,false> vtype, ac_int<VLLEN,false> vl) {
  ac_int<32,false> instruction = rvvInterface.instruction;
  dctoGet->crashFlag = false;
  dctoGet->instruction = instruction;
  dctoGet->r1 = rvvInterface.r1;
  dctoGet->r2 = rvvInterface.r2;
  
  dctoGet->opcode   = instruction.slc<7>(0);
  dctoGet->vd       = instruction.slc<5>(7);
  dctoGet->funct3   = instruction.slc<3>(12);
  dctoGet->vs1      = instruction.slc<5>(15);
  dctoGet->imm5     = instruction.slc<5>(15);
  dctoGet->vs2      = instruction.slc<5>(20);
  dctoGet->lsumop   = instruction.slc<5>(20);
  dctoGet->imm10    = instruction.slc<10>(20);
  dctoGet->vm       = instruction.slc<1>(25);
  dctoGet->mop      = instruction.slc<2>(26);
  dctoGet->funct6   = instruction.slc<6>(26);
  dctoGet->mew      = instruction.slc<1>(28);
  dctoGet->nf       = instruction.slc<3>(29);
  dctoGet->vsetform = instruction.slc<2>(30);

  switch (dctoGet->opcode) {
  case LOAD_FP:
    dctoGet->memType = LOAD;
    break;
  case STORE_FP:
    dctoGet->memType = STORE;
    break;
  default:
    dctoGet->memType = NONE;
    break;
  }
  dctoGet->newInstr = true;

  //bool issetvl = (dctoGet->opcode==OP_V) && (dctoGet->funct3==F3_VSETVL);
  ac_int<3,false> rlmul = ( VTYPE_GET_VLMUL==0 ? 0 :      // lmul-1
                            VTYPE_GET_VLMUL==1 ? 1 : 3 );
  ac_int<2,false> rsew = vtype.slc<2>(3);
  dctoGet->root = 0;
  
  if (dctoGet->opcode==OP_V) {
    switch (dctoGet->funct3) {
    case F3_OPIVV:
    case F3_OPMVV:
      dctoGet->opeType = VV;
      break;
    case F3_OPIVI:
      dctoGet->opeType = VI;
      break;
    case F3_OPIVX:
    case F3_OPMVX:
      dctoGet->opeType = VX;
      break;
    case F3_OPFVF:
    case F3_OPFVV:
      dctoGet->crashFlag = true;
      break;
    }

    switch (dctoGet->funct3) {
    case F3_OPIVI:
      if (dctoGet->funct6 == F6_VMVnrR) {
        dctoGet->wbvtype = false;
	dctoGet->neg1 = false;
	dctoGet->aluOpe = ALU_MERGE;
	dctoGet->vmwb = true;
	dctoGet->vmop = false;
	dctoGet->wbvd = true;
	dctoGet->wbrd = false;
        dctoGet->eew = rsew;
        dctoGet->emul = dctoGet->vs1.slc<3>(0);
        dctoGet->evl = ((VLEN/8)>>dctoGet->eew)<<(dctoGet->emul+1);
        break;
      }
    case F3_OPIVV:
    case F3_OPIVX:
      switch (dctoGet->funct6) {
      case F6_VADD:
        dctoGet->wbvtype = false;
	dctoGet->neg1 = false;
	dctoGet->aluOpe = ALU_ADD;
	dctoGet->vmwb = true;
	dctoGet->vmop = false;
	dctoGet->wbvd = true;
	dctoGet->wbrd = false;
        dctoGet->eew = rsew;
        dctoGet->emul = rlmul;
        dctoGet->evl = vl;
	break;
      case F6_VSUB:
        dctoGet->wbvtype = false;
	dctoGet->neg1 = true;
	dctoGet->swapOp12 = false;
	dctoGet->aluOpe = ALU_ADD;
	dctoGet->vmwb = true;
	dctoGet->vmop = false;
	dctoGet->wbvd = true;
	dctoGet->wbrd = false;
        dctoGet->eew = rsew;
        dctoGet->emul = rlmul;
        dctoGet->evl = vl;
	break;
      case F6_VRSUB:
        dctoGet->wbvtype = false;
	dctoGet->neg1 = true;
	dctoGet->swapOp12 = true;
	dctoGet->aluOpe = ALU_ADD;
	dctoGet->vmwb = true;
	dctoGet->vmop = false;
	dctoGet->wbvd = true;
	dctoGet->wbrd = false;
        dctoGet->eew = rsew;
        dctoGet->emul = rlmul;
        dctoGet->evl = vl;
	break;
      case F6_VMERGE:
        dctoGet->wbvtype = false;
	dctoGet->neg1 = false;
	dctoGet->swapOp12 = false;
	dctoGet->aluOpe = ALU_MERGE;
	dctoGet->vmwb = false;
	dctoGet->vmop = true;
	dctoGet->wbvd = true;
	dctoGet->wbrd = false;
        dctoGet->eew = rsew;
        dctoGet->emul = rlmul;
        dctoGet->evl = vl;
	break;
      default:
        dctoGet->crashFlag = true;
        break;
      }
      dctoGet->wbvtype = false;
      break;
    case F3_OPMVV:
      if (dctoGet->funct6 == F6_VWXUNARY0) {
        switch (dctoGet->vs1) {
        case 0x00:                      // vmv.x.s
          dctoGet->wbvtype = false;
          dctoGet->wbvd = false;
          dctoGet->wbrd = true;
          dctoGet->neg1 = false;
          dctoGet->swapOp12 = true;
          dctoGet->aluOpe = ALU_MERGE;
          dctoGet->vmwb = false;
          dctoGet->vmop = false;
          dctoGet->eew = rsew;
          dctoGet->emul = 0;
          dctoGet->evl = 1;
          break;
        case 0x10:                      // vpopc
        case 0x11:                      // vfirst
        default:
          dctoGet->crashFlag = true;
          break;
        }
        break;
      }
      // fallthrough
    case F3_OPMVX:
      switch (dctoGet->funct6) {
      case F6_VRXUNARY0:                // vmv.s.x
        dctoGet->wbvtype = false;
        dctoGet->wbvd = true;
        dctoGet->wbrd = false;
	dctoGet->neg1 = false;
	dctoGet->swapOp12 = false;
	dctoGet->aluOpe = ALU_MERGE;
	dctoGet->vmwb = false;
	dctoGet->vmop = false;
        dctoGet->eew = rsew;
        dctoGet->emul = 0;
        dctoGet->evl = 1;
        break;
      default:
        dctoGet->crashFlag = true;
        break;
      }
      break;
    case F3_VSETVL:
      dctoGet->aluOpe = ALU_VSETVL;
      dctoGet->wbvtype = true;
      dctoGet->wbrd = true;
      dctoGet->wbvd = false;
      dctoGet->eew = 2;
      dctoGet->emul = 0;
      break;
    }
  }
  dctoGet->isValid = true;
}

void get(DCtoGet dctoGet, GettoEx* gettoEx, ac_int<VLEN,false> v_reg[32], bool* rvvStall) {
  static bool newInstr = true;
  static ac_int<VLEN,false> v0, tmpop1, tmpop2;
  static ac_int<5,false> vs1, vs2, vd;
  static ac_int<3,false> loopcount;
  static ac_int<VLLEN,false> root;

  if (dctoGet.isValid) {
    cout << " v get v " << endl;
    cout << "newinstr" << newInstr << endl;
  if (newInstr) {
    v0 = v_reg[0];
    vs1 = dctoGet.vs1;
    vs2 = dctoGet.vs2;
    vd = dctoGet.vd;
    root = dctoGet.root;
    loopcount = dctoGet.emul;
    newInstr = false;
  } else {
    switch (dctoGet.eew) {
    case 0:
      v0 = v0>>(VLEN/8);
      root+=VLEN/8;
      break;
    case 1:
      v0 = v0>>(VLEN/16);
      root+=VLEN/16;
      break;
    default:
      v0 = v0>>(VLEN/32);
      root+=VLEN/32;
      break;
    }
    vs1++;
    vs2++;
    vd++;
  }
  if (loopcount==0) {
    *rvvStall = false;
    newInstr = true;
  } else {
    *rvvStall = true;
    loopcount--;
  }
  switch (dctoGet.opeType) {
  case VV:
    tmpop1 = v_reg[vs1];
    break;
  case VI:
    tmpop1 = extend((ac_int<XLEN,false>)((ac_int<5,true>)dctoGet.imm5), dctoGet.eew);
    break;
  case VX:
    tmpop1 = extend(dctoGet.r1, dctoGet.eew);
    break;
  }

  tmpop2 = v_reg[vs2];
  
  if (dctoGet.swapOp12) {
    gettoEx->op1 = tmpop2;
    gettoEx->op2 = tmpop1;
  } else {
    gettoEx->op1 = tmpop1;
    gettoEx->op2 = tmpop2;
  }

  gettoEx->op3 = v_reg[vd];

  if (dctoGet.vm)
    gettoEx->m0 = -1;
  else
    gettoEx->m0 = makeMask(v0, dctoGet.eew);
  if (dctoGet.vl>dctoGet.vstart)
    gettoEx->mb = makeMaskBody(root, dctoGet.vstart, dctoGet.evl, dctoGet.eew);
  else
    gettoEx->mb = 0; // Si vstart>=vl, on n'écrit rien dans les registres

  if (dctoGet.vsetform==2) {
    gettoEx->vtype = dctoGet.r2;
  } else {
    gettoEx->vtype = 0;
    gettoEx->vtype.set_slc<10>(0, dctoGet.imm10);
  }
  switch (dctoGet.vsetform) {
  case 0x3:
    gettoEx->avl = dctoGet.imm5;
    break;
  case 0x0:
  case 0x1:
  case 0x2:                      // rd    rs1 | AVL value    (p26)
    if (dctoGet.vs1!=0)          // -     !x0 | x[rs1]
      gettoEx->avl = dctoGet.r1;  //           |
    else if (dctoGet.vd!=0)      // !x0   x0  | ~0
      gettoEx->avl = ~0;          //           |
    else                         // x0    x0  | vl
      gettoEx->avl = dctoGet.vl;
    break;
  }

  gettoEx->neg1 = dctoGet.neg1;
  gettoEx->opeType = dctoGet.opeType;
  gettoEx->aluOpe = dctoGet.aluOpe;

  gettoEx->memType = dctoGet.memType;
  
  gettoEx->vd = vd;
  gettoEx->vmop = dctoGet.vmop;
  gettoEx->vmwb = dctoGet.vmwb;
  gettoEx->wbvtype = dctoGet.wbvtype;
  gettoEx->wbrd = dctoGet.wbrd;
  gettoEx->wbvd = dctoGet.wbvd;
  gettoEx->eew = dctoGet.eew;
  cout << "loop: "<<loopcount << " " << vs1 << " " << vs2 << " " << vd << endl;
  }
  gettoEx->isValid = dctoGet.isValid;
}

// Crée un vecteur de VLEN-bits à partir d'un nombre 32-bits en le recopiant
//   décalé de SEW-bits.
ac_int<VLEN,false> extend(ac_int<XLEN,false> nb, ac_int<2,false> eew) {
  ac_int<VLEN,false> retour;
  for (int i=0; i<VLEN/32; i++) {
    switch (eew) {
    case 0:
      retour.set_slc<8>(32*i,    (ac_int<8,false>)nb);
      retour.set_slc<8>(32*i+8,  (ac_int<8,false>)nb);
      retour.set_slc<8>(32*i+16, (ac_int<8,false>)nb);
      retour.set_slc<8>(32*i+24, (ac_int<8,false>)nb);
      break;
    case 1:
      retour.set_slc<16>(32*i,    (ac_int<16,false>)nb);
      retour.set_slc<16>(32*i+16, (ac_int<16,false>)nb);
      break;
    case 2:
      retour.set_slc<32>(32*i, (ac_int<32,false>)nb);
      break;
    }
  }
  return retour;
}

// Crée un masque de longueur VLEN aligné avec les opérandes à partir de v0
// m0[i] = 1 : actif, m0[i] = 0 : inactif
ac_int<VLEN,false> makeMask(ac_int<VLEN/2,false> v0, ac_int<2,false> eew) {
  ac_int<VLEN,false> m0 = 0;
  for (int i=0; i<VLEN/32; i++) {
    ac_int<32,false> sm = 0;
    switch (eew) {
    case 0:
      sm.set_slc<8>(0,  (ac_int<8>)((ac_int<1,true>)v0.slc<1>(4*i)));
      sm.set_slc<8>(8,  (ac_int<8>)((ac_int<1,true>)v0.slc<1>(4*i+1)));
      sm.set_slc<8>(16, (ac_int<8>)((ac_int<1,true>)v0.slc<1>(4*i+2)));
      sm.set_slc<8>(24, (ac_int<8>)((ac_int<1,true>)v0.slc<1>(4*i+3)));
      break;
    case 1:
      sm.set_slc<16>(0,  (ac_int<16>)((ac_int<1,true>)v0.slc<1>(2*i)));
      sm.set_slc<16>(16, (ac_int<16>)((ac_int<1,true>)v0.slc<1>(2*i+1)));
      break;
    default:
      sm.set_slc<32>(0, (ac_int<32>)((ac_int<1,true>)v0.slc<1>(i)));
      break;
    }
    m0.set_slc<32>(32*i, sm);
  }
  return m0;
}

// Crée un masque de longueur VLEN aligné avec les opérandes.
// mb[i] = 1 : corps, mb[i] = 0 : prestart ou queue
// A optimiser. Synthèse : 0.48ns, 259µm²
ac_int<VLEN,false> makeMaskBody(ac_int<VLLEN,false> root,
                                ac_int<VLLEN,false> vstart,
                                ac_int<VLLEN,false> evl, ac_int<2,false> eew) {
  ac_int<VLEN,false> mb = 0;
  int word_width = (eew==0) ? 8 : (eew==1) ? 16 : 32;
  ac_int<VLENP2,false> firstbit = (vstart<=root) ? ((ac_int<VLENP2,false>)0) : ((ac_int<VLENP2,false>)((vstart-root)*word_width));
  ac_int<VLENP2,false> lastbit = (evl>=root+VLEN) ? ((ac_int<VLENP2,false>)VLEN) : ((ac_int<VLENP2,false>)((evl-root)*word_width));
  for (int i=0; i<VLEN; i+=32) {
    ac_int<32,false> sm = 0;
    switch (eew) {
    case 0:
      sm.set_slc<8>(0 ,(ac_int<8>)((ac_int<1>)( i>=firstbit && i<=lastbit )));
      sm.set_slc<8>(8 ,(ac_int<8>)((ac_int<1>)( i+8>=firstbit && i+8<=lastbit )));
      sm.set_slc<8>(16,(ac_int<8>)((ac_int<1>)( i+16>=firstbit && i+16<=lastbit )));
      sm.set_slc<8>(24,(ac_int<8>)((ac_int<1>)( i+24>=firstbit && i+24<=lastbit )));
      /*
      sm.set_slc<8>(0,  (ac_int<8>)((ac_int<1>)( 4*i+root>=vstart &&
						 4*i+root<evl          )));
      sm.set_slc<8>(8,  (ac_int<8>)((ac_int<1>)( 4*i+1+root>=vstart &&
						 4*i+1+root<evl        )));
      sm.set_slc<8>(16, (ac_int<8>)((ac_int<1>)( 4*i+2+root>=vstart &&
						 4*i+2+root<evl        )));
      sm.set_slc<8>(24, (ac_int<8>)((ac_int<1>)( 4*i+3+root>=vstart &&
      4*i+3+root<evl        )));*/
        break;
      case 1:
      sm.set_slc<16>(0 ,(ac_int<16>)((ac_int<1>)( i>=firstbit && i<=lastbit )));
      sm.set_slc<16>(16,(ac_int<16>)((ac_int<1>)( i+16>=firstbit && i+16<=lastbit )));
      /*sm.set_slc<16>(0,  (ac_int<16>)((ac_int<1>)( 2*i+root>=vstart &&
						   2*i+root<evl          )));
      sm.set_slc<16>(16, (ac_int<16>)((ac_int<1>)( 2*i+1+root>=vstart &&
      2*i+1+root<evl        )));*/
      break;
      default:
      sm.set_slc<32>(0 ,(ac_int<32>)((ac_int<1>)( i>=firstbit && i<=lastbit )));
      /*sm.set_slc<32>(0, (ac_int<32>)((ac_int<1>)( i+root>=vstart &&
        i+root<evl        )));*/
      break;
      }
    mb.set_slc<32>(i, sm);
  }
  return mb;
}

void execute(GettoEx gettoEx, ExtoMem* extoMem) {
  ac_int<VLEN,false> op1, op2;
  ac_int<VLEN,false> m0_op;
  if (gettoEx.isValid) {
    cout << " v execute v " << endl;
  if (gettoEx.neg1)
    op1 = complt2(gettoEx.op1, gettoEx.eew);
  else
    op1 = gettoEx.op1;
  
  op2 = gettoEx.op2;

  m0_op = gettoEx.m0 & gettoEx.mb;
  
  switch (gettoEx.aluOpe) {
  case ALU_VSETVL:
    extoMem->res = setvl(gettoEx.vtype, gettoEx.avl, &(extoMem->vtype));
    break;
  case ALU_ADD:
    extoMem->res = add(op1, op2, gettoEx.eew);
    break;
  case ALU_MERGE:
    extoMem->res = (op1 & m0_op) | (op2 & ~m0_op);
    break;
  }
  if (gettoEx.vmwb)
    extoMem->m0 = m0_op;
  else
    extoMem->m0 = gettoEx.mb;
  
  extoMem->memType = gettoEx.memType;
  
  extoMem->vd = gettoEx.vd;
  extoMem->op3 = gettoEx.op3;
  extoMem->wbvtype = gettoEx.wbvtype;
  extoMem->wbvd = gettoEx.wbvd;
  extoMem->wbrd = gettoEx.wbrd;
  extoMem->eew = gettoEx.eew;
  }
  extoMem->isValid = gettoEx.isValid;
}

void memAccess(ExtoMem extoMem, MemtoWB* memtoWB) {
  memtoWB->res = extoMem.res;
  memtoWB->vd = extoMem.vd;
  memtoWB->m0 = extoMem.m0;
  memtoWB->op3 = extoMem.op3;
  memtoWB->vtype = extoMem.vtype;
  memtoWB->wbvtype = extoMem.wbvtype;
  memtoWB->wbvd = extoMem.wbvd;
  memtoWB->wbrd = extoMem.wbrd;
  memtoWB->eew = extoMem.eew;
  memtoWB->isValid = extoMem.isValid;
}

void writeBack(MemtoWB memtoWB, WBOut* wbOut,
               ac_int<VLEN,false> v_reg[32], ac_int<XLEN,false>* vtype,
               ac_int<VLLEN,false>* vl) {
  ac_int<32,true> rd_value;
  if (memtoWB.isValid) {
    cout << " v writeback v " << endl;
  rd_value = memtoWB.res.slc<XLEN>(0);
  switch (memtoWB.eew) {
  case 0:
    rd_value <<= 24;
    rd_value >>= 24;
    break;
  case 1:
    rd_value <<= 16;
    rd_value >>= 16;
    break;
  }
  if (memtoWB.wbvd) {
    v_reg[memtoWB.vd] = ((memtoWB.res & memtoWB.m0) |
			 (memtoWB.op3 & (~memtoWB.m0)));
  }
  if (memtoWB.wbvtype) {
    *vtype = memtoWB.vtype;
    *vl = memtoWB.res.slc<VLLEN>(0);
  }
  wbOut->rd = rd_value;
  }
  wbOut->wbrd = memtoWB.wbrd;
  wbOut->isValid = memtoWB.isValid;
}

ac_int<VLEN,false> complt2(ac_int<VLEN,false> op1, ac_int<2,false> eew) {
  ac_int<9, false> carry;
  ac_int<1,false> trash;
  op1 = ~op1;
  for (int i=0; i<VBUSW; i+=32) {
    ac_int<32,false> A = op1.slc<32>(i);
    ac_int<32,false> temp = var_adder(A, (ac_int<32,false>)0, 1, &trash, &trash, eew);
    op1.set_slc<32>(i,temp);
  }
  return op1;
}

ac_int<VLEN,false> add(ac_int<VLEN,false> op1,ac_int<VLEN,false> op2,
                       ac_int<2,false> eew) {
  ac_int<9,false> carry;
  ac_int<VLEN, false> retour;
  ac_int<1,false> trash;
  for (int i=0; i<VBUSW; i+=32) {
    ac_int<32,false> A = op1.slc<32>(i);
    ac_int<32,false> B = op2.slc<32>(i);
    ac_int<32,false> temp = var_adder(A, B, 0, &trash, &trash, eew);
    retour.set_slc<32>(i, temp);
  }
  return retour;
}

ac_int<VLEN,false> setvl(ac_int<XLEN,false> vtypein,
                         ac_int<VLLEN,false> avl,
                         ac_int<XLEN,false>* vtypeout) {
  ac_int<3,false> vlmul, vsew;
  vlmul = vtypein.slc<3>(0);
  vsew = vtypein.slc<3>(3);
  if (vlmul>=4 || vsew>=3) {
    vtypein.set_slc<1>(XLEN-1,(ac_int<1,false>)1);  // vill
  }
  *vtypeout = vtypein;

  ac_int<VLEN,false> vl;
  ac_int<VLLEN,false> vlmax = (((VLEN/8)>>vsew)<<vlmul);  // VLMAX=vlen*lmul/sew
  if (avl<=vlmax)
    vl = avl;
  else
    vl = vlmax;

  return vl;
}

// A corriger : synthétise en deux cycles
void doCycle(RV32V* rv32v, bool globalStall) {
  DCtoGet dctoGet_temp;
  GettoEx gettoEx_temp;
  ExtoMem extoMem_temp;
  MemtoWB memtoWB_temp;
  WBOut   wbOut_temp;
  
  decode(*(rv32v->rvvInterface), &dctoGet_temp, rv32v->vtype, rv32v->vl);
  get(rv32v->dctoGet, &gettoEx_temp, rv32v->v_reg, &(rv32v->rvvStall));
  execute(rv32v->gettoEx, &extoMem_temp);
  memAccess(rv32v->extoMem, &memtoWB_temp);
  writeBack(rv32v->memtoWB, &wbOut_temp, rv32v->v_reg, &(rv32v->vtype), &(rv32v->vl));
  if (!globalStall && !(rv32v->rvvStall) && !(rv32v->aluStall) && !(rv32v->memStall)) {
    rv32v->dctoGet = dctoGet_temp;
  }
  if (!globalStall && !(rv32v->aluStall) && !(rv32v->memStall)) {
    rv32v->gettoEx = gettoEx_temp;
    rv32v->extoMem = extoMem_temp;
  }
  if (!globalStall && !(rv32v->memStall)) {
    rv32v->memtoWB = memtoWB_temp;
  }
  if (wbOut_temp.isValid && wbOut_temp.wbrd) {
    rv32v->rvvInterface->rd = wbOut_temp.rd;
  }
  cout << globalStall << rv32v->rvvStall << endl;
}

