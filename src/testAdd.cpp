#include "rv32v.h"

ac_int<VLEN,false> add(ac_int<VLEN,false> op1,
		       ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype);
ac_int<VLEN,false> add32(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype);
ac_int<VLEN,false> add16(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype);
ac_int<VLEN,false> add8(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype);

// Additionneur à largeur variable
// Peu efficace, voir include/var_add.h

ac_int<VLEN,false> add(ac_int<VLEN,false> op1,
		       ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype) {
  ac_int<9,false> carry = 0;
  ac_int<VLEN, false> retour = 0;
  for (int i=0; i<VBUSW; i+=32) {
    carry = op1.slc<8>(i) + op2.slc<8>(i);
    retour.set_slc<8>(i, carry.slc<8>(0));
		      
    carry = (op1.slc<8>(i+8) + op2.slc<8>(i+8) +
	     (carry.slc<1>(8) && VTYPE_GET_VSEW!=0));
    retour.set_slc<8>(i+8, carry.slc<8>(0));
		      
    carry = (op1.slc<8>(i+16) + op2.slc<8>(i+16) +
	     (carry.slc<1>(8) && VTYPE_GET_VSEW==2));
    retour.set_slc<8>(i+16, carry.slc<8>(0));
		      
    carry = (op1.slc<8>(i+24) + op2.slc<8>(i+24) +
	     (carry.slc<1>(8) && VTYPE_GET_VSEW!=0));
    retour.set_slc<8>(i+24, carry.slc<8>(0));
  }
  return retour;
}

ac_int<VLEN,false> add32(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype) {
  ac_int<VLEN, false> retour;
  for (int i=0; i<VBUSW; i+=32) {
    retour.set_slc<32>(i, (ac_int<32,false>)
                          (op1.slc<32>(i) + op2.slc<32>(i)));
  }
  return retour;
}
ac_int<VLEN,false> add16(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype) {
  ac_int<VLEN, false> retour;
  for (int i=0; i<VBUSW; i+=16) {
    retour.set_slc<16>(i, (ac_int<16,false>)
                          (op1.slc<16>(i) + op2.slc<16>(i)));
  }
  return retour;
}

ac_int<VLEN,false> add8(ac_int<VLEN,false> op1,
			ac_int<VLEN,false> op2, ac_int<XLEN,false> vtype) {
  ac_int<VLEN, false> retour = 0;
  for (int i=0; i<VBUSW; i+=8) {
    retour.set_slc<8>(i, (ac_int<8,false>)
                          (op1.slc<8>(i) + op2.slc<8>(i)));
  }
  return retour;
}

