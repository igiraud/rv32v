#include "rv32v.h"

void doCore(RVVInterface* interface, bool globalStall,
            bool* rvvStall, bool* crashFlag) {
  RV32V rv32v;
  initRV32V(&rv32v);
  
  //IncompleteMemory<4> dmInterface = IncompleteMemory<4>(dmData);
  rv32v.rvvInterface = interface;
  //core.data_mem = &dmInterface;
  
  int cycle = 0;
  while(!rv32v.dctoGet.crashFlag) {
    cycle++;
    doCycle(&rv32v, globalStall);
    
    *crashFlag = rv32v.dctoGet.crashFlag;
    *rvvStall = rv32v.rvvStall;
  }
  return;
}
