#include "rv32v.h"

// Multiplieur à largeur variable

ac_int<2*VLEN,false> mul_long(ac_int<VLEN,false> op1,
                              ac_int<VLEN,false> op2,
                              ac_int<XLEN,false> vtype) {
  ac_int<2*VLEN,false> retour;
  for (int i=0; i<VLEN; i+=32) {
    ac_int<2*XLEN,false> part0 = 0;
    ac_int<2*XLEN,false> part1 = 0;
    ac_int<2*XLEN,false> part2 = 0;
    ac_int<2*XLEN,false> part3 = 0;
    ac_int<2*XLEN,false> part4 = 0;
    ac_int<2*XLEN,false> part5 = 0;
    ac_int<2*XLEN,false> part6 = 0;
    ac_int<2*XLEN,false> sum;
    ac_int<16,false> tmp;
    switch (VTYPE_GET_VSEW) {
    case 2:
      tmp = op1.slc<8>(i) * op2.slc<8>(i+24); 
      part1.set_slc<16>(24, tmp);
      
      tmp = op1.slc<8>(i+24) * op2.slc<8>(i);
      part2.set_slc<16>(24, tmp);
      
      tmp = op1.slc<8>(i+8) * op2.slc<8>(i+16);
      part3.set_slc<16>(24, tmp);
      
      tmp = op1.slc<8>(i+16) * op2.slc<8>(i+8);
      part4.set_slc<16>(24, tmp);
      
      tmp = op1.slc<8>(i) * op2.slc<8>(i+16);
      part5.set_slc<16>(16, tmp);
      
      tmp = op1.slc<8>(i+8) * op2.slc<8>(i+24);
      part5.set_slc<16>(32, tmp);
      
      tmp = op1.slc<8>(i+16) * op2.slc<8>(i);
      part6.set_slc<16>(16, tmp);
      
      tmp = op1.slc<8>(i+24) * op2.slc<8>(i+8);
      part6.set_slc<16>(32, tmp);
      // fallthrough
    case 1:
      tmp = op1.slc<8>(i) * op2.slc<8>(i+8);
      part1.set_slc<16>(8, tmp);
      
      tmp = op1.slc<8>(i+16) * op2.slc<8>(i+24);
      part1.set_slc<16>(40, tmp);
      
      tmp = op1.slc<8>(i+8) * op2.slc<8>(i);
      part2.set_slc<16>(8, tmp);
      
      tmp = op1.slc<8>(i+24) * op2.slc<8>(i+16);
      part2.set_slc<16>(40, tmp);
      // fallthrough
    case 0:
      tmp = op1.slc<8>(i) * op2.slc<8>(i);
      part0.set_slc<16>(0, tmp);
      
      tmp = op1.slc<8>(i+8) * op2.slc<8>(i+8);
      part0.set_slc<16>(16, tmp);
      
      tmp = op1.slc<8>(i+16) * op2.slc<8>(i+16);
      part0.set_slc<16>(32, tmp);
      
      tmp = op1.slc<8>(i+24) * op2.slc<8>(i+24);
      part0.set_slc<16>(48, tmp);
      break;
    }
    sum = part0 + part1 + part2 + part3 + part4 + part5 + part6;
    retour.set_slc<2*XLEN>(2*i, sum);
  }
  return retour;
}

ac_int<2*VLEN,false> mul_long32(ac_int<VLEN,false> op1,
                                ac_int<VLEN,false> op2,
                                ac_int<XLEN,false> vtype) {
  ac_int<2*VLEN,false> retour;
  for (int i=0; i<VLEN; i+=32) {
    ac_int<2*32,false> prod = op1.slc<32>(i) * op2.slc<32>(i);
    retour.set_slc<2*32>(2*i,prod);
  }
  return retour;
}

ac_int<2*VLEN,false> mul_long16(ac_int<VLEN,false> op1,
                                ac_int<VLEN,false> op2,
                                ac_int<XLEN,false> vtype) {
  ac_int<2*VLEN,false> retour;
  for (int i=0; i<VLEN; i+=16) {
    ac_int<2*16,false> prod = op1.slc<16>(i) * op2.slc<16>(i);
    retour.set_slc<2*16>(2*i,prod);
  }
  return retour;
}

ac_int<2*VLEN,false> mul_long8(ac_int<VLEN,false> op1,
                                ac_int<VLEN,false> op2,
                                ac_int<XLEN,false> vtype) {
  ac_int<2*VLEN,false> retour;
  for (int i=0; i<VLEN; i+=8) {
    ac_int<2*8,false> prod = op1.slc<8>(i) * op2.slc<8>(i);
    retour.set_slc<2*8>(2*i,prod);
  }
  return retour;
}
