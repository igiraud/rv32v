#include "rv32v.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <ctime>
#include "memoryInterface.h"

#include "print_decode.h"

using namespace std;

#define DES_VL 12
#define DEST_VD 0

int main(int argc, char** argv) {
  RVVInterface interface;
  RV32V proc;
  initRV32V(&proc);
  FILE* fptr;
  uint32_t x_reg[32] = {0};
  proc.rvvInterface = &interface;

  if (argc>=2) {
    fptr = fopen(argv[1], "rb");
  } else {
    fprintf(stderr, "Fournissez un fichier d'instructions\n");
    return -1;
  }
  if (fptr==NULL) {
    fprintf(stderr, "Impossible d'ouvrir %s\n", argv[1]);
    return -1;
  }

  uint32_t instruction;
  ac_int<32,false> ac_instr;
  int i=0;
  while (!proc.dctoGet.crashFlag && i<10) {
    if (!proc.rvvStall) {
      if (!fread(&instruction, sizeof(instruction), 1, fptr) ) {
        fprintf(stderr,"Erreur de lecture\n");
        break;
      }
    }
    printf("%4u : 0x", ++i);
    print_decode(instruction);
    ac_instr = instruction;
    interface.instruction = ac_instr;
    interface.rd = x_reg[ac_instr.slc<5>(7)];
    interface.r1 = x_reg[ac_instr.slc<5>(15)];
    interface.r2 = x_reg[ac_instr.slc<5>(20)];
    doCycle(&proc, false);
    if(proc.dctoGet.crashFlag)
      fprintf(stderr, "Instruction illégale\n");
    cout << "proc.vtype=" <<hex<<proc.vtype<<dec << endl;
  }

  return 0;
}
