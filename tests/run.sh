#!/usr/bin/sh
#
# Usage : ./run.sh <nb>
# génère un fichier elf pouvant être exécuté par spike contenant
# nb instructions risc-v vectorielles pseudo-aléatoires

RISCV="/home/igiraud/riscv-gnu-toolchain/build_rvv/"

make clean
make NB=$1
./test
echo "objcopy"
$RISCV/bin/riscv32-unknown-elf-objcopy -I binary -O elf32-littleriscv --rename-section .data=.text out.bin out.o
echo "ld"
$RISCV/bin/riscv32-unknown-elf-ld --entry=_binary_out_bin_start -T link.ld out.o -o out.elf
