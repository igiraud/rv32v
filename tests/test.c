#include "opcodes.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define MASK 0xFC00707F // Conserve les champs f6, f3 et opcode
#define F6_VSETVLI  0x00
#define F6_VSETIVLI 0x30
#define F6_VSETVL   0x20

#define codev(f6, f3) (f6<<26 | f3<<12 | OP_V)

uint32_t codes[] = {codev(F6_VSETVLI,F3_VSETVL),
                    codev(F6_VSETIVLI,F3_VSETVL),
                    codev(F6_VSETVL,F3_VSETVL),
                    codev(F6_VADD,F3_OPIVV),
                    codev(F6_VADD,F3_OPIVI),
                    codev(F6_VADD,F3_OPIVX),
                    codev(F6_VSUB,F3_OPIVV),
                    codev(F6_VSUB,F3_OPIVI),
                    codev(F6_VSUB,F3_OPIVX),
                    codev(F6_VRSUB,F3_OPIVV),
                    codev(F6_VRSUB,F3_OPIVI),
                    codev(F6_VRSUB,F3_OPIVX),
                    codev(F6_VMERGE,F3_OPIVV),
                    codev(F6_VMERGE,F3_OPIVI),
                    codev(F6_VMERGE,F3_OPIVX),
                    codev(F6_VWXUNARY0,F3_OPMVV),
                    codev(F6_VRXUNARY0,F3_OPMVX),
};

int main() {
  FILE *fptr;
  uint32_t instr, rando;
  int code;
  uint32_t buffer[NB];
  srand(time(NULL));
  printf("%lu\n%8x\n",sizeof(codes)/sizeof(*codes), MASK);
  
  code = rand() % (2); // Pour que la première instruction soit un vsetvl
  rando = rand();
  instr = (rando & (~MASK)) | codes[code];
  printf("%4d : %3u, %08x\n",0, code, instr);
  buffer[0] = instr;

  for (int i=1; i<NB; i++) {
    code = rand() % (sizeof(codes)/sizeof(*codes));
    rando = rand();
    instr = (rando & (~MASK)) | codes[code];
    printf("%4d : %3u, %08x\n",i, code, instr);
    buffer[i] = instr;
  }

  fptr = fopen("out.bin","wb");
  fwrite(buffer,sizeof(*buffer),sizeof(buffer)/sizeof(*buffer),fptr);
  return 0;
}
